<?php

/** @author Kirill A.Lapchinsky rumatakira74@gmail.com
 *  @copyright 2020 Kirill A.Lapchinsky All Rights Reserved
 */

use React\EventLoop\Factory;
use React\Socket\Server;
use React\Socket\ConnectionInterface;

require  'vendor/autoload.php';

$servername = "host";
$username = "user";
$password = "pass";

global $conn, $queue;

// create DB
try {
    $conn = new PDO("mysql:host=$servername", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // create database if not exists
    $sql = "CREATE DATABASE IF NOT EXISTS test2 
            CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
    // use exec() because no results are returned
    $conn->exec($sql);
    $conn->query("use test2");

    // create table if not exists
    $sql = "CREATE TABLE IF NOT EXISTS message (
        id INT PRIMARY KEY AUTO_INCREMENT,
        message_text TEXT NOT NULL) ENGINE=INNODB;";
    $conn->exec($sql);
} catch (PDOException $e) {
    echo $sql . "\n" . $e->getMessage() . "\n";
}

// create queue PHP DS extention
$queue = new \Ds\Queue();

// create socket server
$loop = Factory::create();
$socket = new Server('127.0.0.1:9502', $loop);

$socket->on('connection', function (ConnectionInterface $connection) {
    $connection->on('data', function ($data) use ($connection) {
        global $queue;
        // respond to telnet
        $connection->write("Received: $data");
        // push to queue
        $queue->push($data);
    });
});

echo "Listening on {$socket->getAddress()}\n";

// add timer
$loop->addPeriodicTimer(60, function () {
    global $conn, $queue;
    if (!$queue->isEmpty()) {
        $messageArray = $queue->toArray();
        // insert into DB
        foreach ($messageArray as $value) {
            try {
                $insertion = strval($value);
                $sql = "INSERT INTO message (message_text) 
VALUES (?)";
                $conn->prepare($sql)->execute([$insertion]);
                $queue->clear();
            } catch (PDOException $e) {
                echo $sql . "\n" . $e->getMessage() . "\n";
            }
        }
    }
});

// run loops
$loop->run();
