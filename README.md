**Задание:**
Написать сокет-сервер, который принимает сообщения, сохраняет в очередь. Раз в минуту очередь должна сохраняться в субд. Выбор технологий на выбор исполнителя.

-------


###### Окружение: 
MySQL  8.0.20;

PHP 7.4.6 
extension=ds.so

Telnet


```
git clone

composer install

chmod +x send_messages.sh

```

Bash скрипт send_messages.sh 2 минуты, раз в 10 секунд отправляет на websocket сервер сообщение даты и время.


Запустить сервер: 

`php test2.php`

из другого окна(вкладки) консоли:

`./send_messages.sh | telnet`

Результирующие скрины в соответсвующей папке screenshots.

**Не забудьте изменить на свои параметры доступа к MySQL**

$servername = "host";
$username = "user";
$password = "pass";